# Use case 04: inform IPAM about new IP provided by cloud on resource

# terraform plan -target=null_resource.ex04
# terraform apply -auto-approve -target=null_resource.ex04
# terraform.exe destroy -auto-approve -target=solidserver_ip_address.SX01P05

# create an IP address for a future host linked to the existing space
resource "solidserver_ip_address" "SX01P05" {
  space   = "${solidserver_ip_space.blog.name}"
  subnet  = "${solidserver_ip_subnet.PAR-VPC01-b.name}"
  name    = "sx01p05.blog19"
  request_ip = "10.128.0.34"
}

output "SX01P05-ip" {
  value = "SX01P05: ${solidserver_ip_address.SX01P05.address}"
}

resource "null_resource" "ex04" {
  depends_on = [
                 "solidserver_ip_address.SX01P05"
               ]
}
